# PlAgent 👑

Pluggable **Node.JS framework** to build **feature rich** applications.

**PlAgent** follows the ideas of **DDD** (domain-driven design), **SOLID** principles and **plugin** architecture.

## Installation

`yarn add plagent`

or

`npm i plagent`.

## Documentation

**WIP** 🚧

upgrade 82939123
