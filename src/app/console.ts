import '../lib/core/register-plagent'
import bootstrap from '../bootstrap'
import { PlusAgentFramework } from '../lib/core/PlusAgentFramework'
import { AppContainer } from '../lib/core/di/AppContainer'
import { ConsoleRunner } from '../lib/console/ConsoleRunner'

const main = async () => {
  PlusAgentFramework.requirePlugins('console')
  bootstrap()

  await PlusAgentFramework.run()
  await AppContainer.get(ConsoleRunner).run()
}

main().then()
