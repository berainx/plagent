import '../lib/core/register-plagent'
import bootstrap from '../bootstrap'
import { AppModule } from './app.module'
import { PlusAgentFramework } from '../lib/core/PlusAgentFramework'
import { AppContainer } from '../lib/core/di/AppContainer'
import { NestExpressRunner } from '../lib/web-nest-express/NestExpressRunner'

const main = async () => {
  PlusAgentFramework.requirePlugins('web-nest-express')
  bootstrap()

  await AppContainer.get(NestExpressRunner).run(AppModule)
}

main().then()
