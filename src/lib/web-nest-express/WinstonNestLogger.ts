import { Logger } from '@nestjs/common'
import * as winston from 'winston'

import { NestLoggerContext } from './NestLoggerContext'

import { ReplaceSingleton } from '../core/di/annotations/ReplaceSingleton'
import { AppConfigurator } from '../core/config/AppConfigurator'
import { AppLoggerTkn } from '../logging/plagent-logging-tokens'
import { AppPathUtil } from '../core/config/AppPathUtil'
import { IAppLogger } from '../logging/IAppLogger'
import { Cached } from '../core/lang/annotations/Cached'
import { AppEnv } from '../core/config/AppEnv'

@ReplaceSingleton(AppLoggerTkn)
export class WinstonNestLogger implements IAppLogger {
  public error (message: string, trace: string) {
    if (message) {
      const ctx = this.processContext
      this.errorFileLogger.log('error', message, { trace, at: new Date().toLocaleString(), traceID: ctx?.traceID, path: ctx?.path })
    }

    Logger.error(message, trace)
  }

  public log (message: any, context?: string): void {
    if (message && this.logInfoToFile) {
      const ctx = this.processContext
      this.infoFileLogger.log('info', message, { at: new Date().toLocaleString(), traceID: ctx?.traceID })
    }

    Logger.log(message, context)
  }

  public warn (message: any, context?: string): any {
    if (message && this.logInfoToFile) {
      const ctx = this.processContext
      this.infoFileLogger.log('warn', message, { at: new Date().toLocaleString(), traceID: ctx?.traceID })
    }

    Logger.warn(message, context)
  }

  public debug (message: any): void {
    if (AppEnv.inProduction) {
      return
    }

    if (message && this.logInfoToFile) {
      const ctx = this.processContext
      this.infoFileLogger.log('debug', message, { at: new Date().toLocaleString(), traceID: ctx?.traceID })
    }

    Logger.debug(message)
  }

  @Cached()
  private get logInfoToFile () {
    return AppConfigurator.get<boolean>('web.logInfoToFile')
  }

  private get processContext () {
    return NestLoggerContext.getContext()
  }

  private get dateForPath () {
    const date = new Date()
    const year = date.getFullYear().toString().substring(2)
    const month = date.getMonth() < 9 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1 // отсчет с 0
    return `${year}_${month}`
  }

  @Cached()
  private get errorFileLogger () {
    const logsPath = AppPathUtil.appLogs
    return winston.createLogger({
      format: winston.format.prettyPrint(),
      transports: [ new winston.transports.File({ filename: `${logsPath}/${this.dateForPath}/error.log`, level: 'error' }) ]
    })
  }

  @Cached()
  private get infoFileLogger () {
    const logsPath = AppPathUtil.appLogs
    return winston.createLogger({
      format: winston.format.prettyPrint(),
      transports: [ new winston.transports.File({ filename: `${logsPath}/${this.dateForPath}/info.log`, level: 'info' }) ]
    })
  }
}
