import { Injectable, NestMiddleware } from '@nestjs/common'
import { Request, Response } from 'express'
import { NestLoggerContext } from './NestLoggerContext'
import { v4 as uuid } from 'uuid'

@Injectable()
export class NestLoggerContextMiddleware implements NestMiddleware {
  use (req: Request, res: Response, next: () => void) {
    NestLoggerContext.setContext({ traceID: uuid(), path: req.originalUrl })
    next()
  }
}
