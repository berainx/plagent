import { AsyncLocalStorage } from 'async_hooks'
import { SingletonService } from '../core/di/annotations/SingletonService'

@SingletonService()
export class NestLoggerContext {
  private static localStorage = new AsyncLocalStorage<{ traceID: string, path: string}>()

  public static getContext () {
    return NestLoggerContext.localStorage.getStore()
  }

  public static setContext (store: { traceID: string, path: string }) {
    return NestLoggerContext.localStorage.enterWith(store)
  }
}