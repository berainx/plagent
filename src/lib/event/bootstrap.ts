import { PlusAgentFramework } from '../core/PlusAgentFramework'

export default () => {
  PlusAgentFramework.requirePlugins('logging')
}
