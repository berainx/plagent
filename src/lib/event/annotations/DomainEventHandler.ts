import { TClass } from '../../core/di/plagent-di'
import { IDomainEventHandler } from '../IDomainEventHandler'
import { TDomainEventType } from '../plagent-event'
import { AppContainer } from '../../core/di/AppContainer'
import { DomainEventBusTkn } from '../plagent-event-tokens'
import { IDomainEvent } from '../IDomainEvent'

export const DomainEventHandler = <E extends IDomainEvent, T extends IDomainEventHandler<E>> (type: TDomainEventType) => {
  return (target: TClass<T>) => {
    AppContainer.get(DomainEventBusTkn).listen(type, new target())
  }
}
