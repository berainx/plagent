import { PlusAgentFramework } from '../core/PlusAgentFramework'

export default () => {
  PlusAgentFramework.requirePlugins('console', 'key-value-typeorm')
  require('./console/AppInitController')
}
