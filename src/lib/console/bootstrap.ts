import { PlusAgentFramework } from '../core/PlusAgentFramework'
import { AppEnv } from '../core/config/AppEnv'

export default () => {
  AppEnv.setLaunchType('console')
  PlusAgentFramework.requirePlugins('logging')
}

