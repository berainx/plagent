import { PlusAgentFramework } from '../core/PlusAgentFramework'

export default () => {
  PlusAgentFramework.requirePlugins('mutex')
  require('./cache-manager/CacheManagerAppCache')
  require('./fs/FsAppFileCache')
}

