import { TBaseContextInfo } from './plagent-context-info'
import { TServiceId } from '../di/plagent-di'

export interface IContextService <C extends TBaseContextInfo = TBaseContextInfo> {
  configure (context: C | undefined): this
  createService <T extends IContextService> (id: TServiceId<T>): T
  createContextService <T extends IContextService> (id: TServiceId<T>): T
  contextInfo?: C
}

