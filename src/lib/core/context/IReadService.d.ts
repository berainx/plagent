import { IContextService } from './IContextService'
import { TBaseContextInfo } from './plagent-context-info'

export interface IReadService <C extends TBaseContextInfo = TBaseContextInfo> extends IContextService<C> {}
