import { inject } from 'inversify'
import { TServiceId } from '../plagent-di'

export const InjectService = <T>(id: TServiceId<T>) => {
  return inject(id)
}
