import { PlusAgentFramework } from '../core/PlusAgentFramework'

export default () => {
  PlusAgentFramework.requireModules('persistence')
}
