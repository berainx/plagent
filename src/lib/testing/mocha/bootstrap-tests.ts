import '../../core/register-plagent'
import * as sinon from 'sinon'
import { use } from 'chai'
import * as chaiAsPromised from 'chai-as-promised'
import { AppPathUtil } from '../../core/config/AppPathUtil'
import { AppConfigurationError } from '../../core/application-errors/AppConfigurationError'
import { PlusAgentFramework } from '../../core/PlusAgentFramework'

use(chaiAsPromised)

let bootstrapApp: (() => void) | undefined
try {
  bootstrapApp = require(AppPathUtil.appSrc + '/bootstrap').default
} catch (e) {
  throw e
  // No bootstrap file in "src" folder found
}

if (!bootstrapApp) {
  throw new AppConfigurationError('Cannot find application bootstrap file. Tests cannot be started')
}

bootstrapApp()

before(async function () {
  await PlusAgentFramework.run()
})

after(async function () {
  await PlusAgentFramework.shutdown()
})

afterEach(() => {
  // Restore the default sandbox
  sinon.restore()
})
