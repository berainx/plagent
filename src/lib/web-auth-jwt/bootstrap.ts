import { PlusAgentFramework } from '../core/PlusAgentFramework'

export default () => {
  PlusAgentFramework.requirePlugins('web', 'auth', 'crypto')

  require('./jwt/JwtService')
}
