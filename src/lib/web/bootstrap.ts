import { PlusAgentFramework } from '../core/PlusAgentFramework'
import { AppEnv } from '../core/config/AppEnv'

export default () => {
  AppEnv.setLaunchType('web')
  PlusAgentFramework.requirePlugins('logging', 'cache')
}
