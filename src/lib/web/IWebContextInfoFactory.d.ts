import { TContextAuth } from '../auth/auth-context'
import { TBaseContextInfo } from '../core/context/plagent-context-info'
import { TAnyRequest } from './plagent-web'

export interface IWebContextInfoFactory <C extends TBaseContextInfo = TBaseContextInfo> {
  getContext (req: TAnyRequest, auth: TContextAuth | undefined): C
}
