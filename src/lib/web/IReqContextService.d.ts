import { TAnyRequest } from './plagent-web'
import { TBaseContextInfo } from '../core/context/plagent-context-info'

export interface IReqContextService {
  writeContextToReq (req: TAnyRequest): void | Promise<void>
  readContextFromReq <C extends TBaseContextInfo> (req: TAnyRequest): C
}
