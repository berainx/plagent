import { PlusAgentFramework } from '../core/PlusAgentFramework'
import { PersistenceTypeormConfig } from '../persistence-typeorm/PersistenceTypeormConfig'
import { KeyValueTypeormConfig } from './KeyValueTypeormConfig'

export default () => {
  const connectionName = KeyValueTypeormConfig.inst.useWithConnection
  PersistenceTypeormConfig.addConnectionEntity(connectionName, __dirname + '/model')
  PlusAgentFramework.requirePlugins('key-value', 'persistence-typeorm')

  require('./typeorm/KeyValueTypeormStorage')
}
