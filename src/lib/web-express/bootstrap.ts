import { PlusAgentFramework } from '../core/PlusAgentFramework'

export default () => {
  PlusAgentFramework.requirePlugins('web', 'logging')
  require('./ExpressLogger')
  require('./ExpressWebFacade')
}
