import { PlusAgentFramework } from '../core/PlusAgentFramework'

export default () => {
  PlusAgentFramework.requirePlugins('persistence')
  require('./DefaultTypeormConnection')
}
