import { IEntityManager } from './IEntityManager'
import { IRepository } from './IRepository'
import { IHasId } from './IHasId'
import { TPersistenceId } from './plagent-persistence'
import { TServiceId } from '../core/di/plagent-di'

export interface IRepositoryFactory {
  get <T extends IRepository<E, ID>, E extends IHasId<ID>, ID extends TPersistenceId> (
    token: TServiceId<T>, transactionalEm?: IEntityManager
  ): T
}
