import { TPersistenceId } from './plagent-persistence'

export interface IHasId <ID extends TPersistenceId = number> {
  getId (): ID
}
