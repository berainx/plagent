import * as fs from 'fs'
import { AppConfigurator } from './lib/core/config/AppConfigurator'
import { AppPathUtil } from './lib/core/config/AppPathUtil'
import { KeyValueTypeormConfig } from './lib/key-value-typeorm/KeyValueTypeormConfig'
import { PlusAgentFramework } from './lib/core/PlusAgentFramework'
import { PersistenceTypeormConfig } from './lib/persistence-typeorm/PersistenceTypeormConfig'

export default () => {
  const config = fs.readFileSync(AppPathUtil.appData + '/.appconfig').toString()
  AppConfigurator.importConfig(config)

  PersistenceTypeormConfig.useDefaultConnection(false)
  KeyValueTypeormConfig.useWithConnection('test')

  PlusAgentFramework.requirePlugins('logging', 'key-value-typeorm', 'event-local', 'validation-yup', 'mutex')
  PlusAgentFramework.requireModules('main')
}
